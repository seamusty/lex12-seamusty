#include "schemeValidator.h"

int schemeValidator(char scheme[]) {
  int result = -1;

  if (isAlpha(scheme[0])) { // changed to 0 for indexing. 
    if(isUpper(scheme[0])){
      result = 1;
    }
    else if('a' <= scheme[0] && scheme[0] <= 'z'){
      result = 0;
    }
     
    int i = 1;
    while (scheme[i] != '\0') {
      if((result != -1) && (isUpper(scheme[i]))){
        result = 1;
      }  
      if (!isAlpha(scheme[i])){
	 result = -1;
      }	
      else if (!isDigit(scheme[i])){ 
	result = -1;
      }	
      else if (!isSpecial(scheme[i])){ 
	result = -1;
      }
      i++;	
    }
    return result;
  }
  else{
    return -1; // first character is not alphabetical.
  }
}

_Bool isUpper(char c) {
  if('A' <= c && c <= 'Z'){
    return true;
  }
  else
    return false;
}


_Bool isAlpha(char c) {
  if('a' <= c && c <= 'z'){
    return true;
  }
  else if('A' <= c && c <= 'Z'){
    return true;
  }
  else
    return false;
}

_Bool isSpecial(char c) {
  if (c == '+'){
    return true;
  }
  else if (c == '-'){ 
    return true;
  }
  else if (c == '.'){
    return true;
  }
  else
    return false;
}

_Bool isDigit(char c) {
  if(0 <= c && c <= 9){
    return true;
  }
  else
    return false;
}


